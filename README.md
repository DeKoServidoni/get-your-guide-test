# GetYourGuide Test

First of all, thanks a lot for this opportunity to show a little my technical skills in Android development. This was a simple test so i added some others complexities build a better test for you guys.

### Architecture
For the architecture i choose to follow the MVVM pattern, together with databinding, RxJava and Architecture components like LiveData and ViewModel. But why to choose this one with all this components?

In my opinion for every application we need to think about an architecture and the MVVM provides me good practices for it. We can make the code more testable (using unit tests) and maintainable (easy to modify when needed).

![alt text](GetYourGuide-Android-Arch.png "MVVM")

## Features
The test consists of the following features:

- Retrieve data from API.
- Pagination to retrieve pages of data of size 20
- Filter by traveler type
- Sort by asc and desc date
- Sort by asc and desc rating
- Handle network status
- ViewModel Unit tests

#### Components
- **DataBinding**: Choosed because it's avoid lot's of boilerplate with findViewById and that we can "bind" the view model with the view directly in XML.
- **RxJava**: Choosed to do the communication between data layer (network) with the ViewModel and it need to be testable so we avoid to send context to the ViewModel.
- **LiveData**: Choosed to do the communication between the ViewModel and the View (activities, fragments). The fact that it is lifecycle aware we need a lifecycle oberver (with context) so the activity handle all the steps to finish the "connection" with it.

#### Libraries
- **Support components**,
- **ArchComponents**: LiveData, ViewModel and Paging (for pagination),
- **Dagger**: To remove boilerplate and be more testable
- **Retrofit**: To do the network work),
- **RxJava**
- **OMFM**: My own library of a float action button menu, so i think to use to handle the sort types of the list, can be found at: https://github.com/DeKoServidoni/OMFM,
- **Mockito & MockServer**: For unit tests, to mock classes and webservers.

#### Challenges
The biggest challenge in the test was using the new Paging component from google, to handle the pagination. I think to user because i was studing it some weeks ago. 
But if was needed to do in the "not component" way, i will start to adding an addOnScrollListener in the Recycler view to handle last visible item and the total item count to start to fetch the next page if exists. Besides that i will add a footer on the recyclerview too, to show the loading to the user when the next pages was loading. 

So i think this is all my comments.<br>Thanks again for this opportunity and hope you all appreciate my work :)

Together in the package is the architecture diagram for this test (the same showed above)

Regards,<br>
André Servidoni




