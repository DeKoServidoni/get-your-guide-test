package com.dekoservidoni.getyourguidetest

import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.views.viewmodels.ReviewRowViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ReviewRowViewModelTest : BaseTest() {

    private lateinit var viewModel: ReviewRowViewModel

    companion object {
        private const val EXPECTED_TITLE = "Awesome"
        private const val EXPECTED_COMMENT = "Really clear and informative tour guide. Great to access the building. Wish the tour was longer as there is so much to take in. Thank you."
        private const val EXPECTED_AUTHOR = "Andre – Brazil"
        private const val EXPECTED_DATE = "September 22, 2018"
        private const val EXPECTED_RATING = 5.0
    }

    @Before
    fun setup() {
        viewModel = ReviewRowViewModel(buildTestData())
    }

    @Test
    fun testDataFormat() {
        Assert.assertTrue("Invalid hour format!", viewModel.getTitle() == EXPECTED_TITLE)
        Assert.assertTrue("Invalid hour format!", viewModel.getComment() == EXPECTED_COMMENT)
        Assert.assertTrue("Invalid hour format!", viewModel.getAuthor() == EXPECTED_AUTHOR)
        Assert.assertTrue("Invalid hour format!", viewModel.getDate() == EXPECTED_DATE)
        Assert.assertTrue("Invalid hour format!", viewModel.getRating() == EXPECTED_RATING)
    }

    /// Private methods

    private fun buildTestData(): Review = Review(3782783,
            EXPECTED_TITLE, EXPECTED_COMMENT, EXPECTED_AUTHOR, EXPECTED_DATE, EXPECTED_RATING)
}