package com.dekoservidoni.getyourguidetest

import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.view.View
import com.dekoservidoni.getyourguidetest.data.GetYourGuideNetwork
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.models.ReviewsResponse
import com.dekoservidoni.getyourguidetest.models.status.NetworkStatus
import com.dekoservidoni.getyourguidetest.views.ReviewsRepository
import com.dekoservidoni.getyourguidetest.views.viewmodels.ReviewsViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.mockwebserver.MockResponse
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.io.InputStreamReader

class ReviewsViewModelTest: BaseTest() {

    private var service = retroFit.create(GetYourGuideNetwork::class.java)
    private var repository = ReviewsRepository(service)

    @Mock
    private lateinit var contentObserver: Observer<PagedList<Review>>

    @Mock
    private lateinit var dataObserver: Observer<NetworkStatus>

    private lateinit var viewModel: ReviewsViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testFetchSuccess() {
        val response = buildTestData()
        mockServerResponse(200, response)

        viewModel = ReviewsViewModel(repository)
        viewModel.data.observeForever(dataObserver)

        viewModel.contentData.observeForever {
            Assert.assertNotNull(it)
            Assert.assertTrue(it?.size == response.data.size)
            Assert.assertTrue("Visibility of the content must be VISIBLE!", viewModel.getContentVisibility() == View.VISIBLE)
            Assert.assertTrue("Visibility of the progress must be GONE!", viewModel.getProgressVisibility() == View.GONE)
        }
    }

    @Test
    fun testFetchError() {
        mockServerResponse(403, null)

        viewModel = ReviewsViewModel(repository)
        viewModel.data.observeForever(dataObserver)
        viewModel.contentData.observeForever(contentObserver)

        Assert.assertTrue("Visibility of the content must be VISIBLE!", viewModel.getContentVisibility() == View.VISIBLE)
        Assert.assertTrue("Visibility of the progress be GONE!", viewModel.getProgressVisibility() == View.GONE)

        Mockito.verify(dataObserver).onChanged(ArgumentMatchers.eq(NetworkStatus.Error("HTTP 403 Client Error")))
    }

    @After
    fun finishTest() {
        server.shutdown()
    }

    /// Private methods

    private fun mockServerResponse(responseCode: Int, responseModel: ReviewsResponse?) {
        val mockResult = Gson().toJson(responseModel)
        val mockResponse = MockResponse().setResponseCode(responseCode).setBody(mockResult)
        server.enqueue(mockResponse)
    }

    private fun buildTestData(): ReviewsResponse {
        val inputStream = this@ReviewsViewModelTest::class.java.classLoader?.getResourceAsStream("reviews_response.json")
        val listType = object : TypeToken<ReviewsResponse>() {}.type
        return Gson().fromJson<ReviewsResponse>(InputStreamReader(inputStream), listType)
    }
}