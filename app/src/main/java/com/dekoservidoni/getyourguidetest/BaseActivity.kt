package com.dekoservidoni.getyourguidetest

import android.support.design.widget.Snackbar
import android.view.View
import com.dekoservidoni.getyourguidetest.utils.NetworkLifecycleObserver
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.processors.PublishProcessor
import java.util.*

abstract class BaseActivity: DaggerAppCompatActivity() {

    protected var networkListener: PublishProcessor<Boolean>? = null
    private var snackBar: Snackbar? = null

    protected fun listenToNetworkChanges(publisher: PublishProcessor<Boolean>) {
        NetworkLifecycleObserver(this, this, publisher)
    }

    protected fun showSnack(root: View, message: String?, actionListener: View.OnClickListener?) {
        buildSnack(root, message, actionListener, R.string.retry)
    }

    protected fun dismissSnack() {
        snackBar?.dismiss()
    }

    private fun buildSnack(root: View, message: String?, actionListener: View.OnClickListener?, action: Int) {
        if (snackBar != null) {
            snackBar?.dismiss()
        }

        snackBar = Snackbar.make(root, "%s".format(Locale.US, message), Snackbar.LENGTH_LONG)

        if (actionListener != null) {
            snackBar?.setAction(action, actionListener)
            snackBar?.duration = Snackbar.LENGTH_INDEFINITE
        }

        snackBar?.show()
    }
}