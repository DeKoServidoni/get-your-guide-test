package com.dekoservidoni.getyourguidetest.models


data class ReviewParams(
        var direction: String = "DESC",
        var sortBy: String = "date_of_review"
)