package com.dekoservidoni.getyourguidetest.models

import com.google.gson.annotations.SerializedName

data class ReviewsResponse(
        @SerializedName("status")
        val status: Boolean,

        @SerializedName("total_reviews_comments")
        val total: Int,

        @SerializedName("data")
        val data: ArrayList<ReviewItem> = ArrayList()
)

data class ReviewItem(
        @SerializedName("review_id")
        val id: Long?,

        @SerializedName("rating")
        val rating: String?,

        @SerializedName("title")
        val title: String?,

        @SerializedName("message")
        val message: String?,

        @SerializedName("author")
        val author: String?,

        @SerializedName("foreignLanguage")
        val foreignLanguage: Boolean?,

        @SerializedName("date")
        val date: String?,

        /** In the response JSON always came like: "date_unformatted": {} **/
        //@SerializedName("date_unformatted")
        //val dateUnformatted: Any?,

        @SerializedName("languageCode")
        val languageCode: String?,

        @SerializedName("traveler_type")
        val travelerType: String?,

        @SerializedName("reviewerName")
        val reviewerName: String?,

        @SerializedName("reviewerCountry")
        val reviewerCountry: String?
)