package com.dekoservidoni.getyourguidetest.models

data class Review(
        val id: Long?,
        val title: String?,
        val message: String?,
        val author: String?,
        val date: String?,
        val rating: Double?
)