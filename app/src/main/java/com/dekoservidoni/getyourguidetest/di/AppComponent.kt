package com.dekoservidoni.getyourguidetest.di

import com.dekoservidoni.getyourguidetest.BaseApp
import com.dekoservidoni.getyourguidetest.di.modules.ActivityBuildersModule
import com.dekoservidoni.getyourguidetest.di.modules.AppModule
import com.dekoservidoni.getyourguidetest.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBuildersModule::class,
    ViewModelModule::class])
interface AppComponent: AndroidInjector<BaseApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BaseApp): AppComponent.Builder
        fun build(): AppComponent
    }

    override fun inject(application: BaseApp)
}