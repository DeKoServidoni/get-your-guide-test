package com.dekoservidoni.getyourguidetest.di.modules

import com.dekoservidoni.getyourguidetest.views.ReviewsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector()
    abstract fun bindMainActivity(): ReviewsActivity
}