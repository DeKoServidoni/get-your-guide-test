package com.dekoservidoni.getyourguidetest.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.dekoservidoni.getyourguidetest.di.AppViewModelFactory
import com.dekoservidoni.getyourguidetest.di.ViewModelKey
import com.dekoservidoni.getyourguidetest.views.viewmodels.ReviewsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ReviewsViewModel::class)
    abstract fun mainViewModel(viewModel: ReviewsViewModel): ViewModel
}