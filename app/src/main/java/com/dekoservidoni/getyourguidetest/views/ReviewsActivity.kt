package com.dekoservidoni.getyourguidetest.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import com.dekoservidoni.getyourguidetest.BaseActivity
import com.dekoservidoni.getyourguidetest.R
import com.dekoservidoni.getyourguidetest.databinding.ActivityReviewsBinding
import com.dekoservidoni.getyourguidetest.models.ReviewParams
import com.dekoservidoni.getyourguidetest.models.status.NetworkStatus
import com.dekoservidoni.getyourguidetest.views.viewmodels.ReviewsViewModel
import com.dekoservidoni.omfm.OneMoreFabMenu
import io.reactivex.processors.PublishProcessor
import kotlinx.android.synthetic.main.activity_reviews.*
import javax.inject.Inject

class ReviewsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(ReviewsViewModel::class.java) }

    private lateinit var binding: ActivityReviewsBinding
    private lateinit var reviewsAdapter: ReviewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_reviews)
        binding.viewModel = viewModel

        setupNetworkObserver()
        setupListener()
        setupUI(binding)
    }

    /// Private methods

    private fun setupNetworkObserver() {
        networkListener = PublishProcessor.create<Boolean>()

        networkListener?.subscribe {
            viewModel.hasConnection = it

            if(it) {
                dismissSnack()
                viewModel.fetchReviews()
            } else {
                showSnack(binding.root, getString(R.string.network_error), null)
            }
        }

        networkListener?.let {
            listenToNetworkChanges(it)
        }
    }

    private fun setupListener() {
        viewModel.contentData.observe(this, Observer {
            reviewsAdapter.submitList(it)
        })

        viewModel.data.observe(this, Observer { response ->
            response?.let {
                when(it) {
                    is NetworkStatus.Error -> showSnack(binding.root, it.error, null)
                }
            }
        })

        binding.filter.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(adapter: AdapterView<*>?) {
                // do nothing
            }

            override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.filterReviews(position)
            }

        }

        binding.fabOptions.setOptionsClick(object: OneMoreFabMenu.OptionsClick {
            override fun onOptionClick(optionId: Int?) {

                optionId?.let {

                    val params = when(it) {

                        R.id.date_asc_option ->
                            ReviewParams("ASC", "date_of_review")

                        R.id.date_desc_option ->
                            ReviewParams("DESC", "date_of_review")

                        R.id.rating_asc_option ->
                            ReviewParams("ASC", "rating")

                        R.id.rating_desc_option ->
                            ReviewParams("DESC", "rating")

                        else -> ReviewParams()
                    }

                    viewModel.fetchReviews(params = params)
                }
            }
        })
    }

    private fun setupUI(binding: ActivityReviewsBinding) {
        reviewsAdapter = ReviewsAdapter()

        with(content) {
            layoutManager = LinearLayoutManager(this@ReviewsActivity)
            adapter = reviewsAdapter
        }

        setSupportActionBar(binding.toolbar)
    }
}
