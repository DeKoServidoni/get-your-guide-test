package com.dekoservidoni.getyourguidetest.views

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dekoservidoni.getyourguidetest.R
import com.dekoservidoni.getyourguidetest.databinding.RowReviewBinding
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.views.viewmodels.ReviewRowViewModel

class ReviewsAdapter: PagedListAdapter<Review, ReviewsAdapter.ServiceViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<RowReviewBinding>(inflater,
                R.layout.row_review, parent, false)

        return ServiceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        val viewModel = ReviewRowViewModel(getItem(position))
        holder.binding.viewModel = viewModel
        holder.binding.executePendingBindings()
    }

    class ServiceViewHolder(val binding: RowReviewBinding): RecyclerView.ViewHolder(binding.root)

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Review>() {
            override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean =
                    oldItem == newItem
        }
    }
}