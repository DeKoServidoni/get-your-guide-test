package com.dekoservidoni.getyourguidetest.views.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.databinding.Bindable
import android.view.View
import com.dekoservidoni.getyourguidetest.BR
import com.dekoservidoni.getyourguidetest.BaseViewModel
import com.dekoservidoni.getyourguidetest.data.GetYourGuideDataSource
import com.dekoservidoni.getyourguidetest.data.GetYourGuideDataSourceFactory
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.models.ReviewParams
import com.dekoservidoni.getyourguidetest.models.status.NetworkStatus
import com.dekoservidoni.getyourguidetest.utils.GetYourGuidePrefs
import com.dekoservidoni.getyourguidetest.views.ReviewsRepository
import javax.inject.Inject


class ReviewsViewModel @Inject constructor(repository: ReviewsRepository,
                                           private val prefs: GetYourGuidePrefs)
    : BaseViewModel<NetworkStatus>() {

    private var progressVisibility = View.VISIBLE
    private var contentVisibility = View.GONE

    private var factory = GetYourGuideDataSourceFactory(repository, compositeDisposable)

    var contentData: LiveData<PagedList<Review>>
        private set

    init {
        compositeDisposable.add(factory.getProcessor().subscribe {
            when(it) {
                is NetworkStatus.Loading -> showLoading(true)
                is NetworkStatus.Success -> showLoading(false)
                is NetworkStatus.Error -> {
                    showLoading(false)
                    data.postValue(NetworkStatus.Error(it.error))
                }
            }
        })

        val pagedListConfig = PagedList.Config.Builder().setEnablePlaceholders(false)
                .setInitialLoadSizeHint(GetYourGuideDataSource.INITIAL_LOADING)
                .setPageSize(GetYourGuideDataSource.PAGE_SIZE)
                .build()

        contentData = LivePagedListBuilder(factory, pagedListConfig).build()
    }

    fun fetchReviews(params: ReviewParams = ReviewParams()) {
        if(hasConnection) {
            factory.setParams(params)
            contentData.value?.dataSource?.invalidate()
        }
    }

    fun filterReviews(selected: Int) {
        prefs.saveFilter(selected)

        if(hasConnection) {
            contentData.value?.dataSource?.invalidate()
        }
    }

    /// Bindable methods

    @Bindable
    fun getProgressVisibility(): Int = progressVisibility

    @Bindable
    fun getContentVisibility(): Int = contentVisibility

    /// Private methods

    private fun showLoading(show: Boolean) {
        progressVisibility = if(show) View.VISIBLE else View.GONE
        contentVisibility = if(show) View.GONE else View.VISIBLE

        notifyPropertyChanged(BR.contentVisibility)
        notifyPropertyChanged(BR.progressVisibility)
    }
}