package com.dekoservidoni.getyourguidetest.views

import com.dekoservidoni.getyourguidetest.data.GetYourGuideNetwork
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.models.ReviewItem
import com.dekoservidoni.getyourguidetest.utils.FilterType
import com.dekoservidoni.getyourguidetest.utils.GetYourGuidePrefs
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class ReviewsRepository @Inject constructor(private val network: GetYourGuideNetwork, private val prefs: GetYourGuidePrefs) {

    fun getReviews(totalPerPage: Int, page: Int, rating: Int, sortBy: String, direction: String): Single<List<Review>> {
        return network
                .requestReviews(count = totalPerPage, page = page, rating = rating, sortBy = sortBy, direction = direction)
                .flatMapPublisher {

                    val content = if (it.status) it.data else ArrayList()

                    Flowable.fromIterable(content)
                            .filter { item ->
                                filterReview(item)
                            }
                }
                .map { mapReview(it) }
                .toList()
    }

    /// Private methods

    private fun mapReview(item: ReviewItem): Review {
        val value = item.rating?.toDouble() ?: 0.0
        return Review(item.id, item.title, item.message, item.author, item.date, value)
    }

    private fun filterReview(item: ReviewItem): Boolean {

        val filterType = prefs.retrieveFilter()

        return filterType?.let {

            when (it) {
                FilterType.SEE_ALL_REVIEWS -> true

                FilterType.COUPLES, FilterType.FAMILY,
                FilterType.GROUP_OF_FRIENDS, FilterType.SOLO_TRAVELER -> item.travelerType == it.value
            }

        } ?: false
    }
}