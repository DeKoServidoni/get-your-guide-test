package com.dekoservidoni.getyourguidetest.views.viewmodels

import com.dekoservidoni.getyourguidetest.models.Review

class ReviewRowViewModel(private val review: Review?) {

    fun getTitle(): String? = review?.title

    fun getComment(): String? = review?.message

    fun getAuthor(): String? = review?.author

    fun getDate(): String? = review?.date

    fun getRating(): Double? = review?.rating
}