package com.dekoservidoni.getyourguidetest.data

import android.arch.paging.DataSource
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.models.ReviewParams
import com.dekoservidoni.getyourguidetest.models.status.NetworkStatus
import com.dekoservidoni.getyourguidetest.views.ReviewsRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor

class GetYourGuideDataSourceFactory(private val repository: ReviewsRepository,
                                    private val compositeDisposable: CompositeDisposable)
    : DataSource.Factory<Int, Review>() {

    private val status = PublishProcessor.create<NetworkStatus>()
    private var params = ReviewParams()

    override fun create(): DataSource<Int, Review> {
        val dataSource = GetYourGuideDataSource(repository, compositeDisposable, params)
        dataSource.status.subscribe { status.onNext(it) }

        return dataSource
    }

    fun setParams(params: ReviewParams) {
        this.params = params
    }

    fun getProcessor(): PublishProcessor<NetworkStatus> = status
}