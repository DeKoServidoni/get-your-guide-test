package com.dekoservidoni.getyourguidetest.data

import android.arch.paging.PageKeyedDataSource
import com.dekoservidoni.getyourguidetest.models.Review
import com.dekoservidoni.getyourguidetest.models.ReviewParams
import com.dekoservidoni.getyourguidetest.models.status.NetworkStatus
import com.dekoservidoni.getyourguidetest.views.ReviewsRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import timber.log.Timber

class GetYourGuideDataSource(private var repository: ReviewsRepository,
                             private var compositeDisposable: CompositeDisposable,
                             private var reviewParams: ReviewParams)
    : PageKeyedDataSource<Int, Review>() {

    var status: PublishProcessor<NetworkStatus> = PublishProcessor.create<NetworkStatus>()
        private set

    companion object {
        const val PAGE_SIZE = 20
        const val INITIAL_LOADING = 20
        const val PAGE_START = 0
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Review>) {
        Timber.e("Initial loading (size of %d)", params.requestedLoadSize)

        status.onNext(NetworkStatus.Loading)

        compositeDisposable.add(repository.getReviews(PAGE_SIZE, PAGE_START, 0,
                reviewParams.sortBy, reviewParams.direction)
                .subscribe({
                    status.onNext(NetworkStatus.Success)
                    callback.onResult(it, null, 1)
                }, {
                    status.onNext(NetworkStatus.Error(it.localizedMessage))
                }))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Review>) {
        Timber.e("AFTER loading: %d (size of %d)", params.key, params.requestedLoadSize)

        compositeDisposable.add(repository.getReviews(PAGE_SIZE, params.key, 0,
                reviewParams.sortBy, reviewParams.direction)
                .subscribe({
                    status.onNext(NetworkStatus.Success)
                    callback.onResult(it, params.key + 1)
                }, {
                    status.onNext(NetworkStatus.Error(it.localizedMessage))
                }))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Review>) {
        // do nothing
    }
}