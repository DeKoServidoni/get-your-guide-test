package com.dekoservidoni.getyourguidetest.data

import com.dekoservidoni.getyourguidetest.models.ReviewsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface GetYourGuideNetwork {

    @Headers("User-Agent: GetYourGuide")
    @GET("{location}/{place}/reviews.json")
    fun requestReviews(
            @Path("location") location: String = "berlin-l17",
            @Path("place") place: String = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776",
            @Query("count") count: Int,
            @Query("page") page: Int,
            @Query("rating") rating: Int,
            @Query("sortBy") sortBy: String,
            @Query("direction") direction: String): Single<ReviewsResponse>
}