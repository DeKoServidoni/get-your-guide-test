package com.dekoservidoni.getyourguidetest.utils

enum class FilterType(val value: String) {
    SEE_ALL_REVIEWS("all"), COUPLES("couple"), FAMILY("family_old"),
    GROUP_OF_FRIENDS("friends"), SOLO_TRAVELER("solo");

    companion object {
        fun fromPosition(position: Int): FilterType {
            return FilterType.values()[position]
        }
    }
}