package com.dekoservidoni.getyourguidetest.utils

import android.databinding.BindingAdapter
import android.widget.RatingBar
import android.widget.TextView
import com.dekoservidoni.getyourguidetest.R

object BindAdapters {

    @JvmStatic
    @BindingAdapter("rating")
    fun setRating(ratingBar: RatingBar, rating: Double?) {
        rating?.let {
            ratingBar.rating = it.toFloat()
        }
    }

    @JvmStatic
    @BindingAdapter("author")
    fun setAuthor(textView: TextView, author: String?) {
        author?.let {
            val context = textView.context
            textView.text = context.getString(R.string.reviewed_by, it)
        }
    }
}