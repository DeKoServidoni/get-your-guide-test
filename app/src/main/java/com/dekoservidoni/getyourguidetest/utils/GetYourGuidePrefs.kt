package com.dekoservidoni.getyourguidetest.utils

import android.content.Context

class GetYourGuidePrefs(context: Context) {

    companion object {
        private const val PREFS = "get-your-guide-prefs"
        private const val FILTER = "filter"
    }

    private var sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    fun saveFilter(filter: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(FILTER, filter)
        editor.apply()
    }

    fun retrieveFilter(): FilterType? {
        val filter = sharedPreferences.getInt(FILTER, 0)
        return FilterType.fromPosition(filter)
    }
}