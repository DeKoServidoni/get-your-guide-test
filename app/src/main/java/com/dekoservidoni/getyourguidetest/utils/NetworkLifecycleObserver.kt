package com.dekoservidoni.getyourguidetest.utils

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import io.reactivex.processors.PublishProcessor
import java.lang.ref.WeakReference

class NetworkLifecycleObserver(lifecycleOwner: LifecycleOwner, context: Context?,
                               publisher: PublishProcessor<Boolean>): LifecycleObserver {

    private val receiver: NetworkChangeReceiver
    private var applicationContext: WeakReference<Context>? = null
    private var isRegistered = false

    init {
        lifecycleOwner.lifecycle.addObserver(this)
        context?.let {
            applicationContext = WeakReference(it)
        }
        receiver = NetworkChangeReceiver(applicationContext?.get(), publisher)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart(owner: LifecycleOwner) {
        if(!isRegistered) {
            applicationContext?.get()?.registerReceiver(receiver, receiver.getFilter())
            isRegistered = true
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop(owner: LifecycleOwner) {
        if(isRegistered) {
            applicationContext?.get()?.unregisterReceiver(receiver)
            isRegistered = false
        }
    }
}